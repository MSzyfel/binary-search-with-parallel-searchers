from GraphWithStrategies import *
import igraph


class Server:
    def __init__(self):
        self.graph_with_strategies = None
        self.searchers = []
        self.query_count = {  # total_queries as a percent of total queries of phi,
            # max_queries as a percent of total queries of phi
            "phi": [0.0, 0.0],
            "gamma": [0.0, 0.0],
            "lambda1": [0.0, 0.0],
            "lambda3": [0.0, 0.0],
        }

    def set_graph_and_create_strategies(self, graph_with_strategy: GraphWithStrategies):
        self.graph_with_strategies = graph_with_strategy
