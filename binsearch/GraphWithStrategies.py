import igraph
from binsearch.Strategy import Strategy
from binsearch.medians import *


class GraphWithStrategies:
    def __init__(self, graph: igraph.Graph):
        self.graph = graph.copy()
        phi_strategy = Strategy(graph, phi)
        gamma_strategy = Strategy(graph, gamma)
        lambda1_strategy = Strategy(graph, lambda1)
        lambda3_strategy = Strategy(graph, lambda3)
        self.strategies = {
            "phi": phi_strategy,
            "gamma": gamma_strategy,
            "lambda1": lambda1_strategy,
            "lambda3": lambda3_strategy,
        }
        self.query_count = {  # total_queries as a percent of total queries of phi,
            # max_queries as a percent of total queries of phi
            "phi": [0.0, 0.0],
            "gamma": [0.0, 0.0],
            "lambda1": [0.0, 0.0],
            "lambda3": [0.0, 0.0],
        }

    def get_graph_size(self):
        return len(self.graph.vs)

    def get_query_count(self):
        return self.query_count

    def get_strategy(self, function: str):
        return self.strategies[function]

    def search(self, target: int, strategy):
        number_of_queries = 0
        graph = self.graph.copy()
        while True:
            query_result = strategy.query(graph, target)
            number_of_queries += 1
            if query_result is True or len(query_result.vs) == 1:
                return number_of_queries
            else:
                graph = query_result

    def create_statistics_for_function(self, function: str):
        strategy = self.strategies[function]
        total_queries = 0
        max_queries = 0
        for target in self.graph.vs:
            current_queries = self.search(target.index, strategy)
            if current_queries > max_queries:
                max_queries = current_queries
            total_queries += current_queries
        self.query_count[function] = [total_queries, max_queries]

    def create_statistics(self):
        self.create_statistics_for_function("phi")
        self.create_statistics_for_function("gamma")
        self.create_statistics_for_function("lambda1")
        self.create_statistics_for_function("lambda3")
        phi_queries = self.query_count["phi"]
        gamma_queries = self.query_count["gamma"]
        lambda1_queries = self.query_count["lambda1"]
        lambda3_queries = self.query_count["lambda3"]
        self.query_count["phi"] = [phi_queries[0]/phi_queries[0], phi_queries[1]/phi_queries[1]]
        self.query_count["gamma"] = [gamma_queries[0]/phi_queries[0], gamma_queries[1]/phi_queries[1]]
        self.query_count["lambda1"] = [lambda1_queries[0]/phi_queries[0], lambda1_queries[1]/phi_queries[1]]
        self.query_count["lambda3"] = [lambda3_queries[0]/phi_queries[0], lambda3_queries[1]/phi_queries[1]]
