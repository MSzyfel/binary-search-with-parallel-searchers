import igraph
import hashlib


class Strategy:
    def __init__(self, graph: igraph.Graph, median_function):
        self.subgraphs = {}
        self.create_strategy(graph, median_function)

    def add_subgraph(self, subgraph, median_vertex_index, sub_graphs_sub_graphs: dict):
        # When creating so-called strategy for each subgraph that is possible to occur
        # as a result of a query we save a calculated median vertex of it because it is
        # independent of query
        hashed_graph = self._hash_graph(subgraph)
        directions = {}
        for neighbor, sub_graph in sub_graphs_sub_graphs.items():
            for vertex in sub_graph.vs:
                vertex_absolute_index = vertex["name"]
                directions[vertex_absolute_index] = neighbor
        self.subgraphs[hashed_graph] = (subgraph, median_vertex_index, directions, sub_graphs_sub_graphs)

    def get_median_vertex_index(self, hashed_graph):
        graph_info = self.subgraphs.get(hashed_graph)
        if graph_info:
            return graph_info[1]  # Return the median vertex
        else:
            return None

    def query(self, subgraph: igraph.Graph, target: int):
        # notice that as a matter of fact that given subgraph may have different
        # indices than other subgraph (indices are always numbers from 0 to subgraph size)
        # we need a way to address to them in an absolute way and therefore have names
        # which are their absolute index (index in original graph)
        # the target parameter is name/absolute index of the target
        hashed_graph = self._hash_graph(subgraph)
        median_index = self.get_median_vertex_index(hashed_graph)
        if subgraph.vs[median_index]["name"] == target:  # to check if we hit the vertex we
            # check its name, not its index
            return True
        else:
            subgraph_index = self.subgraphs[hashed_graph][2][target]
            return self.subgraphs[hashed_graph][3][subgraph_index]

    def _hash_graph(self, graph):
        # Get graph properties
        properties = {
            "edges": sorted(graph.get_edgelist()),
            "vertices": sorted([(v.index, v["name"]) for v in graph.vs]),
            "degree_sequence": sorted(graph.degree()),
            "is_directed": graph.is_directed(),
        }

        # Convert properties to a string
        properties_str = str(properties)

        # Hash the string using SHA-256
        hashed_str = hashlib.sha256(properties_str.encode()).hexdigest()

        return hashed_str

    def divide_graph_by_vertex(self, graph: igraph.Graph, median_relative_index):
        # Find the index of the dividing vertex

        shortest_paths = graph.get_shortest_paths(
            v=median_relative_index, to=None, mode="out"
        )
        shortest_paths.pop(median_relative_index)

        shortest_paths = [[sublist[1], sublist[-1]] for sublist in shortest_paths]

        subsets = {}
        for pair in shortest_paths:
            key = pair[0]
            value = pair[1]
            if key not in subsets:
                subsets[key] = [value]
            else:
                subsets[key].append(value)

        subgraph_dict = {}
        for key, value in subsets.items():
            subgraph_dict[key] = graph.subgraph(value)
        return subgraph_dict

    def create_strategy(self, graph: igraph.Graph, median_function):
        min_vertex = min(graph.vs, key=lambda v: median_function(graph, v.index))
        sub_graphs = self.divide_graph_by_vertex(graph, min_vertex.index)
        for sub_graph in sub_graphs.values():
            if len(sub_graph.vs) == 1:
                self.add_subgraph(sub_graph, sub_graph.vs[0], {})
            else:
                self.create_strategy(sub_graph, median_function)
        self.add_subgraph(graph, min_vertex.index, sub_graphs)
        return
