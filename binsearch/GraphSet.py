import igraph
from binsearch.Strategy import *
from binsearch.GraphWithStrategies import *
from Server import *
from Searcher import *


class GraphSet:
    def __init__(self):
        self.graphs_with_strategies = []
        self.query_count = {  # total_queries per vertex, max_queries per vertex
            "phi": [0.0, 0.0],
            "gamma": [0.0, 0.0],
            "lambda1": [0.0, 0.0],
            "lambda3": [0.0, 0.0],
        }

    def get_query_count(self):
        return self.query_count

    def add_graph(self, graph_with_strategy: GraphWithStrategies):
        self.graphs_with_strategies.append(graph_with_strategy)

    def create_statistics(self):
        for graph_with_strategy in self.graphs_with_strategies:
            graph_with_strategy.create_statistics()
        self.summarize_statistics()

    def summarize_statistics(self):
        for graph_with_strategy in self.graphs_with_strategies:
            graph_size = graph_with_strategy.get_graph_size()
            query_count = graph_with_strategy.get_query_count()
            for key, value in query_count.items():
                self.query_count[key][0] += value[0]
                self.query_count[key][1] += value[1]
        for key, value in self.query_count.items():
            self.query_count[key][0] /= len(self.graphs_with_strategies)
            self.query_count[key][1] /= len(self.graphs_with_strategies)
