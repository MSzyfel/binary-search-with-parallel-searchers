import scipy
import igraph
import math
from collections import Counter


def get_neighbor_set_sizes(graph: igraph.Graph, vertex: int) -> dict[int, int]:
    """Get sizes of sets of vertices from which the shortest paths to
    the given vertex include a specific neighbor of the given vertex,
    one for each neighbor.

    :param graph: graph in which the given vertex is.
    :param vertex: vertex which neighbor sets to get.
    :return: dictionary of (neighbor, neighbor_set_size) pairs.
    """
    paths = graph.get_shortest_paths(v=vertex, to=None, mode="out")
    # Remove the path to itself, which is the only path with length equal to 1
    paths.pop(vertex)
    neighbor_occurrences = Counter(path[1] for path in paths)
    return neighbor_occurrences

def inverse_epsilon(n: int) -> float:
    if n <= 2:
        return 2
    else:
        # Some crazy math magic
        value = scipy.special.lambertw(n * math.log(n / 2), tol=0.01) / math.log(n / 2)
        return value.real


def phi(graph: igraph.Graph, vertex: int) -> int:
    """Function calculating the potential of the given vertex in the graph.

    This function calculates the sum of distances to every vertex
    from the given one.

    :param graph: graph in which the given vertex is.
    :param vertex: vertex of which the potential to calculate.
    :return: the sum of distances to every vertex.
    """
    paths = graph.get_shortest_paths(v=vertex, to=None)
    distances = [len(path) - 1 for path in paths]
    return sum(distances)


def gamma(graph: igraph.Graph, vertex: int) -> int:
    """Function calculating the potential of the given vertex in the graph.

    This function calculates the maximum of the sizes of sets of vertices
    from which the shortest paths to the given vertex contain a specific
    neighbor of the given vertex.

    :param graph: graph in which the given vertex is.
    :param vertex: vertex of which the potential to calculate.
    :return: the maximum of the neighbor set sizes.
    """
    neighbor_set_sizes = get_neighbor_set_sizes(graph, vertex)
    return max(neighbor_set_sizes.values())


def lambda1(graph: igraph.Graph, vertex: int) -> float:
    """Function calculating the potential of the given vertex in the graph.

    TODO: write this docstring

    :param graph: graph in which the given vertex is.
    :param vertex: vertex of which the potential to calculate.
    :return:
    """
    neighbor_set_sizes = get_neighbor_set_sizes(graph, vertex)
    weights = [size * math.log(size) for size in neighbor_set_sizes.values()]
    return sum(weights)


def lambda3(graph: igraph.Graph, vertex: int) -> float:
    """Function calculating the potential of the given vertex in the graph.

    TODO: write this docstring

    :param graph: graph in which the given vertex is.
    :param vertex: vertex of which the potential to calculate.
    :return:
    """
    neighbor_set_sizes = get_neighbor_set_sizes(graph, vertex)
    weights = [
        size * math.log(size) / (math.log(inverse_epsilon(size)))
        for size in neighbor_set_sizes.values()
    ]
    return sum(weights)
