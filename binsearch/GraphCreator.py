import igraph
from binsearch.Strategy import *
from binsearch.GraphWithStrategies import *
from binsearch.GraphSet import *


class GraphCreator:
    def __init__(self):
        self.graph_sets = {}

    def get_sets_query_count(self, descriptor):
        return self.graph_sets[descriptor].get_query_count()

    def create_statistics_for_set(self, descriptor):
        self.graph_sets[descriptor].create_statistics()

    def create_barabasi_set(
        self,
        lower_size_limit: int,
        upper_size_limit: int,
        percent_of_gen_edges: float,
        graphs_per_size: int,
    ):
        new_set = GraphSet()
        for i in range(max(2, lower_size_limit), upper_size_limit):
            for j in range(graphs_per_size):
                graph = igraph.Graph.Barabasi(
                    i, min(i, max(1, round(i * percent_of_gen_edges)))
                )
                while not igraph.Graph.is_connected(graph, "strong"):
                    graph = igraph.Graph.Barabasi(
                        i, min(i, max(1, round(i * percent_of_gen_edges)))
                    )
                graph.vs["name"] = [i for i in range(0, len(graph.vs))]
                graph_with_strategy = GraphWithStrategies(graph)
                new_set.add_graph(graph_with_strategy)
        descriptor = (
            "barabasi",
            max(2, lower_size_limit),
            upper_size_limit,
            percent_of_gen_edges,
        )
        self.graph_sets[descriptor] = new_set
        return descriptor

    def create_erdos_renyi_set(
        self,
        lower_size_limit: int,
        upper_size_limit: int,
        percent_of_gen_edges: float,
        sparse: bool,
        graphs_per_size: int,
    ):
        new_set = GraphSet()
        for i in range(max(2, lower_size_limit), upper_size_limit):
            for j in range(graphs_per_size):
                graph = None
                if sparse:
                    graph = igraph.Graph.Erdos_Renyi(
                        i,
                        min(
                            i * (i - 1) / 2, max(i - 1, round(i * percent_of_gen_edges))
                        ),
                    )
                else:
                    graph = igraph.Graph.Erdos_Renyi(
                        i, max(i - 1, round(i * (i - 1) * percent_of_gen_edges / 2))
                    )
                while not igraph.Graph.is_connected(graph, "strong"):
                    if sparse:
                        graph = igraph.Graph.Erdos_Renyi(
                            i,
                            min(
                                i * (i - 1) / 2,
                                max(i - 1, round(i * percent_of_gen_edges)),
                            ),
                        )
                    else:
                        graph = igraph.Graph.Erdos_Renyi(
                            i, max(i - 1, round(i * (i - 1) * percent_of_gen_edges / 2))
                        )
                graph.vs["name"] = [i for i in range(0, len(graph.vs))]
                graph_with_strategy = GraphWithStrategies(graph)
                new_set.add_graph(graph_with_strategy)
        descriptor = (
            "erdos_renyi",
            max(2, lower_size_limit),
            upper_size_limit,
            percent_of_gen_edges,
            sparse,
        )
        self.graph_sets[descriptor] = new_set
        return descriptor

    def create_tree_set(
        self, lower_size_limit: int, upper_size_limit: int, graphs_per_size: int
    ):
        new_set = GraphSet()
        for i in range(max(2, lower_size_limit), upper_size_limit):
            for j in range(graphs_per_size):
                graph = igraph.Graph.Tree_Game(i)
                while not igraph.Graph.is_connected(graph, "strong"):
                    graph = igraph.Graph.Tree_Game(i)
                graph.vs["name"] = [i for i in range(0, len(graph.vs))]
                graph_with_strategy = GraphWithStrategies(graph)
                new_set.add_graph(graph_with_strategy)
        descriptor = (
            "tree",
            max(2, lower_size_limit),
            upper_size_limit,
        )
        self.graph_sets[descriptor] = new_set
        return descriptor

    def create_ordered_tree_set(
        self,
        lower_size_limit: int,
        upper_size_limit: int,
        children: int,
        graphs_per_size: int,
    ):
        new_set = GraphSet()
        for i in range(max(2, lower_size_limit), upper_size_limit):
            for j in range(graphs_per_size):
                graph = igraph.Graph.Tree(i, max(1, min(i - 1, children)))
                while not igraph.Graph.is_connected(graph, "strong"):
                    graph = igraph.Graph.Tree(i, max(1, min(i - 1, children)))
                graph.vs["name"] = [i for i in range(0, len(graph.vs))]
                graph_with_strategy = GraphWithStrategies(graph)
                new_set.add_graph(graph_with_strategy)
        descriptor = (
            "ordered_tree",
            max(2, lower_size_limit),
            upper_size_limit,
            max(1, children),
        )
        self.graph_sets[descriptor] = new_set
        return descriptor

    def create_k_regular_set(
        self, lower_size_limit: int, upper_size_limit: int, k: int, graphs_per_size: int
    ):
        new_set = GraphSet()
        for i in range(max(2, k + 1, lower_size_limit), upper_size_limit):
            for j in range(graphs_per_size):
                graph = igraph.Graph.K_Regular(n=i, k=max(2, k))
                while not igraph.Graph.is_connected(graph, "strong"):
                    graph = igraph.Graph.K_Regular(n=i, k=max(2, k))
                graph.vs["name"] = [i for i in range(0, len(graph.vs))]
                graph_with_strategy = GraphWithStrategies(graph)
                new_set.add_graph(graph_with_strategy)
        descriptor = (
            "k-regular",
            max(2, k + 1, lower_size_limit),
            upper_size_limit,
            max(2, k),
        )
        self.graph_sets[descriptor] = new_set
        return descriptor
