from Server import *


class Searcher:
    def __init__(self):
        self.position = 0  # absolute position in a graph which is the "name" of a vertex
        self.subgraph = None
        self.number_of_queries = 0
        self.server = None

    def get_position(self):
        return self.position

    def set_server(self, server: Server):
        self.server = server

    def set_position(self, position):
        self.position = position

    def set_subgraph(self, subgraph):
        self.subgraph = subgraph

    def query(self):
        self.number_of_queries = self.number_of_queries + 1
